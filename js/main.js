// Bài 1.
const btnTaxEl = document.getElementById("btnTax");

btnTaxEl.addEventListener("click", function () {
  var inputNameEl = document.getElementById("inputName").value;
  var inputSalaryEl = document.getElementById("inputSalary").value * 1;
  var inputUserEl = document.getElementById("inputUser").value * 1;
  var calTax = inputSalaryEl - 4e6 - 16e5 * inputUserEl;
  var tax = 0;
  if (calTax <= 6e7) {
    tax = calTax * 0.5;
  } else if (calTax <= 12e7) {
    tax = 6e7 * 0.05 + (calTax - 6e7) * 0.1;
  } else if (calTax <= 21e7) {
    tax = 6e7 * 0.05 + 6e7 * 0.1 + (calTax - 12e7) * 0.15;
  } else if (calTax <= 384e6) {
    tax = 6e7 * 0.05 + 6e7 * 0.1 + 9e7 * 0.15 + (calTax - 21e7) * 0.2;
  } else if (calTax <= 624e6) {
    tax =
      6e7 * 0.05 +
      6e7 * 0.1 +
      9e7 * 0.15 +
      174e6 * 0.2 +
      (calTax - 384e6) * 0.25;
  } else if (calTax <= 960e6) {
    tax =
      6e7 * 0.05 +
      6e7 * 0.1 +
      9e7 * 0.15 +
      174e6 * 0.2 +
      24e7 * 0.25 +
      (calTax - 624e6) * 0.3;
  } else {
    tax =
      6e7 * 0.05 +
      6e7 * 0.1 +
      9e7 * 0.15 +
      174e6 * 0.2 +
      24e7 * 0.25 +
      336e6 * 0.3 +
      (calTax - 96e7) * 0.35;
  }
  //   if (calTax <= 6e7) {
  //     tax = calTax * 0.5;
  //   } else if (calTax <= 12e7) {
  //     tax = calTax * 0.1;
  //   } else if (calTax <= 21e7) {
  //     tax = calTax * 0.15;
  //   } else if (calTax <= 384e6) {
  //     tax = calTax * 0.2;
  //   } else if (calTax <= 624e6) {
  //     tax = calTax * 0.25;
  //   } else if (calTax <= 960e6) {
  //     tax = calTax * 0.3;
  //   } else {
  //     tax = calTax * 0.35;
  //   }
  tax = Intl.NumberFormat("vi-VN", {
    style: "currency",
    currency: "VND",
  }).format(tax);
  document.getElementById(
    "txtTax"
  ).innerHTML = `Họ tên: ${inputNameEl}. Tiền thuế thu nhập cá nhân: ${tax}`;
});

// Bài 2
const btnNetEl = document.getElementById("btnNet");
const NO_SECLECT = "noSelect";
const USER = "user";
const COMPANY = "company";

function disableInput() {
  var selCustomerEl = document.getElementById("selCustomer").value;
  document.getElementById("inputConnect").style.display =
    "company" == selCustomerEl ? "block" : "none";
}

btnNetEl.addEventListener("click", function () {
  var selCustomerEl = document.getElementById("selCustomer").value;
  var inputIDEl = document.getElementById("inputID").value;
  var inputChanel = document.getElementById("inputChanel").value * 1;
  var inputConnectEl = document.getElementById("inputConnect").value * 1;
  var result = 0;
  if (selCustomerEl == NO_SECLECT) {
    alert("Hãy chọn loại khách hàng!!!");
  }
  if (selCustomerEl == USER) {
    result = 4.5 + 20.5 + inputChanel * 7.5;
  }
  if (selCustomerEl == COMPANY) {
    if (inputConnectEl <= 10) {
      result = 15 + 75 + inputChanel * 50;
    } else {
      result = 15 + 75 + (inputConnectEl - 10) * 5 + inputChanel * 50;
    }
  }
  result = Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  }).format(result);
  document.getElementById(
    "txtNet"
  ).innerHTML = `Mã khách hàng: ${inputIDEl}; Tiền cáp: ${result}`;
});
